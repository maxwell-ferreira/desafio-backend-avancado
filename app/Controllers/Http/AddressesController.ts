import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Address from 'App/Models/Address'
import { schema, rules } from '@ioc:Adonis/Core/Validator'
import fs from 'fs'
import { parse } from 'fast-csv'
import axios from 'axios'
import { string } from '@ioc:Adonis/Core/Helpers'

export default class AddressesController {

  public async index({ request }: HttpContextContract) {

    const page = request.input('page', 1)
    const perPage = 10

    const addresses = await Address.query().paginate(page, perPage)

    return addresses
  }

  public async store({ request, response }: HttpContextContract) {

    const validateData = await request.validate(this.formSchema)

    const addressData = { ...validateData, longitude: null, latitude: null }
    const coordenates = await this.getAddressCoordenates(validateData)
    if (coordenates) {
      addressData.latitude = coordenates.lat
      addressData.longitude = coordenates.lon
    }

    const address = await Address.create(addressData)

    if (!address.$isPersisted) return { error: "Error when creating. Try again." }

    return response.status(201).json({ address })
  }

  public async show({ params }: HttpContextContract) {
    const id = params.id
    const address = await Address.findOrFail(id)

    return { address }
  }

  public async update({ request, params }: HttpContextContract) {

    const validateData = await request.validate(this.formSchema)

    const id = params.id
    const address = await Address.findOrFail(id)

    const addressData = { ...validateData, longitude: '', latitude: '' }
    const coordenates = await this.getAddressCoordenates(validateData)
    if (coordenates) {
      addressData.latitude = coordenates.lat
      addressData.longitude = coordenates.lon
    }

    await address.merge(addressData).save()

    if (!address.$isPersisted) return { error: "Error when updating. Try again." }

    return { address }
  }

  public async destroy({ params }: HttpContextContract) {
    const id = params.id
    const address = await Address.findOrFail(id)

    await address.delete();

    if (!address.$isDeleted) return { error: "Error when deleting. Try again." }

    return { message: "Successfully deleted." }
  }

  public async insertCsv() {
    var insertData = <any>[];

    const readStream = fs.createReadStream('../../addresses_model.csv')
      .pipe(parse({ headers: true, delimiter: ',' }))

    for await (const row of readStream) {
      insertData.push(row);
    }

    const addresses = await Address.createMany(insertData)

    return { addresses }
  }

  public async interestPoints({ request }: HttpContextContract) {

    const data = await request.validate({
      schema: schema.create({
        latitude: schema.number(),
        longitude: schema.number(),
        radius: schema.number(),
      })
    })

    const page = request.input('page', 1)
    const perPage = 10

    const addresses = await Address.query().whereRaw(`
      (SELECT (6371 *
                acos(
                  cos(radians(:latitude)) *
                  cos(radians(Latitude)) *
                  cos(radians(:longitude) - radians(longitude)) +
                  sin(radians(:latitude)) *
                  sin(radians(latitude))
                )
              )) <= :radius
    `, data).paginate(page, perPage)

    return addresses
  }

  private getAddressCoordenates(addressData) {

    return new Promise((resolve, reject) => {
      const API_URL = `https://nominatim.openstreetmap.org/search?q=${string.encodeSymbols(addressData.number.toString())}+
                        ${string.encodeSymbols(addressData.street)}+${string.encodeSymbols(addressData.neighborhood)}+
                        ${string.encodeSymbols(addressData.city)}+${string.encodeSymbols(addressData.state)}&format=json`

      axios.get(API_URL, {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Accept': 'application/json'
        }
      }).then(data => {
        resolve(data.data[0])

      }).catch(err => reject(err.error))

    })
  }

  private formSchema = {
    schema: schema.create({

      name: schema.string(
        { trim: true },
        [rules.maxLength(255)]
      ),

      street: schema.string(
        { trim: true },
        [rules.maxLength(255)]
      ),

      number: schema.number(),

      neighborhood: schema.string(
        { trim: true },
        [rules.maxLength(255)]
      ),

      state: schema.string(
        { trim: true },
        [rules.maxLength(255)]
      ),

      city: schema.string(
        { trim: true },
        [rules.maxLength(255)]
      ),
    })
  }
}
