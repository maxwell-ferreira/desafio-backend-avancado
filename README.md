# Desafio Backend Avançado Paytime

Projeto desenvolvido com o framwork AdonisJs para o Desafio Backend Avançado da Paytime.

## Descrição

Esta aplicação permite ao usuário cadastrar e gerenciar endereços como pontos de interesse.
	
## Iniciando o projeto!

Para rodar o projeto, siga os passos abaixo:

1. Clone este repositório em sua máquina local, em alguma pasta de interesse.
2. Abra o terminal integrado na pasta aonde clonou o projeto.
   1. Execute o comando: ```npm install``` para instalar as dependências.
   2. Execute o comando: ```node ace serve``` para iniciar o servidor que serve a aplicação.
3. Pronto, agora basta acessar _localhost:3333/docs_ para acessar a documentação da API!

